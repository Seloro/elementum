extends AnimatedSprite2D

func  _process(delta):
	if GLOBAL.contadorContrataque > 7:
		play("3")
	elif GLOBAL.contadorContrataque > 5:
		play("2")
	elif GLOBAL.contadorContrataque > 3:
		play("1")
	else:
		play("0")
