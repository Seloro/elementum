extends StaticBody2D

@export var indice: int
var jugadorArriba: bool
@onready var pocision: Marker2D = $Pocision

func _process(delta):
	infoPociciones()

func _on_arriba_area_entered(area):
	if area.is_in_group("Jugador"):
		jugadorArriba = true
		GLOBAL.arriba = true

func _on_arriba_area_exited(area):
	if area.is_in_group("Jugador"):
		jugadorArriba = false
		GLOBAL.arriba = false

func infoPociciones():
	if jugadorArriba:
		GLOBAL.posicionesJefeFuegoAtaqueEspecial[indice] = Vector2.ZERO
	else:
		GLOBAL.posicionesJefeFuegoAtaqueEspecial[indice] = pocision.global_position
