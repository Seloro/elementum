extends Area2D

var velocidad: int
var fueraDePantalla: bool

func _ready():
	velocidad = randi_range(250, 500)
	if global_rotation != 0:
		$"BalaJefeTierra(normal)".flip_h = true

func _process(delta):
	if !fueraDePantalla:
		if global_rotation != 0:
			position += Vector2.from_angle(global_rotation) * delta * velocidad
		else:
			position.x -= delta * velocidad
	else: 
		queue_free()

func _on_visible_on_screen_notifier_2d_screen_exited():
	fueraDePantalla = true

func _on_area_entered(area):
	if area.name == "Ataque jugador":
		if $"BalaJefeTierra(normal)".flip_h == true:
			$"BalaJefeTierra(normal)".flip_h = false
		var indise: int = randi_range(0, 2)
		velocidad *= -1
		remove_from_group("Bala")
		add_to_group("Ataque jugador")
		if indise == 0:
			global_rotation = 0
			$"BalaJefeTierra(normal)".flip_h = true
		elif  indise == 1:
			global_rotation = randf_range(-3, -3.5)
	if area.is_in_group("Ataque jugador") && is_in_group("Bala"):
		queue_free()
	if area.is_in_group("Bala") && is_in_group("Ataque jugador"):
		queue_free()
	if area.is_in_group("Bala pilar"):
		queue_free()
