extends CharacterBody2D

var velocidad: int = 50
var animacion: AnimatedSprite2D
var movimiento: bool
var x: int
var y: int
@export var balaEsena: PackedScene
var vida: float = 1
var inpacto: Area2D

func _ready():
	animacion = $Forma
	$Ataque.start(randf_range(1, 3))
	$"Camvio de forma".start(randf_range(2, 4))
	inpacto = $Inpacto
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if movimiento && vida > 0:
		position += Vector2(x, y) * delta * velocidad
	move_and_slide()
	if vida <= 0:
		GLOBAL.cantidadDeEnemigos -= 1
		queue_free()

func _on_movimiento_timeout():
	movimiento = !movimiento

func _on_camvio_de_direccion_timeout():
	x = randi_range(-1, 1)
	y = randi_range(-1, 1)

func _on_ataque_timeout():
	if vida > 0:
		var bala = balaEsena.instantiate()
		bala.position = $Mira.global_position
		if randi_range(0, 1):
			$Mira.look_at(GLOBAL.jugador.global_position)
			if randi_range(0, 1):
				bala.rotation = randf_range(3, 3.5)
			else:
				bala.rotation = $Mira.global_rotation
		get_parent().add_child(bala)
		if inpacto.name != "Defensa":
			$Ataque.start(randf_range(1, 3))
		else:
			$Ataque.start(.75)

func _on_camvio_de_forma_timeout():
	if inpacto.name != "Defensa":
		animacion.play("Canvio de forma")
		await (animacion.animation_finished)
		inpacto.name = "Defensa"
		velocidad = 0
		$"Camvio de forma".start(randf_range(4, 8))
	else:
		animacion.play_backwards("Canvio de forma")
		await (animacion.animation_finished)
		inpacto.name = "Inpacto"
		velocidad = 50
		$"Camvio de forma".start(randf_range(2, 4))

func _on_inpacto_area_entered(area):
	if area.is_in_group("Ataque jugador") && inpacto.name != "Defensa":
		area.queue_free()
		vida -= 1
