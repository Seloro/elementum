extends Node2D

@export var balaEsena: PackedScene
var derecha: bool
var puedeAtacar: bool

func _ready():
	derecha = randi_range(0, 1)
	$"Abrir ojo".start(randf_range(.5, 2))
	$"Forma/HieloVivo(ojo)".visible = false

func _process(delta):
	if derecha:
		$Forma.rotation += delta
	else: 
		$Forma.rotation -= delta

func _on_abrir_ojo_timeout():
	$"Forma/HieloVivo(ojo)".visible = true
	$Forma/HieloVivo.visible = false
	$"Disparar y cerrar ojo".start(randf_range(.5, 1))
	puedeAtacar = true

func _on_disparar_y_cerrar_ojo_timeout():
	if puedeAtacar && GLOBAL.vidaJugador > 0:
		if GLOBAL.contadorOla < 80:
			var bala = balaEsena.instantiate()
			bala.position = position
			if randi_range(0, 1):
				$Mira.look_at(GLOBAL.jugador.global_position)
				if randi_range(0, 1):
					bala.rotation = randf_range(3, 3.5)
				else:
					bala.rotation = $Mira.global_rotation
			get_parent().add_child(bala)
			GLOBAL.contadorOla += 1
		puedeAtacar = false
		$"Disparar y cerrar ojo".start(randf_range(.5, 1))
	else:
		$"Forma/HieloVivo(ojo)".visible = false
		$Forma/HieloVivo.visible = true
		$"Abrir ojo".start(randf_range(.5, 2))
