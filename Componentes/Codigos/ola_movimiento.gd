extends Area2D

var velocidad: int
var noVisible: bool
var jugador

func _ready():
	velocidad = randi_range(300, 400)

func _process(delta):
	if !noVisible:
		global_position.x -= delta * velocidad
	else:
		queue_free()
	if jugador != null:
		if jugador.z_index == 0:
			GLOBAL.vidaJugador -= 1
			jugador.position.x -= 20

func _on_visible_screen_exited():
	noVisible = true

func _on_area_entered(area):
	if area.is_in_group("Jugador"):
		jugador = area.get_parent()

func _on_area_exited(area):
	if area.is_in_group("Jugador"):
		jugador = null
