extends Area2D

@export var velocidad: int
var balaTransparente: bool
var furaDePantalla: bool

func _ready():
	$Forma.play("Normal")
	velocidad = randi_range(250, 500)
	$Camvio.start(randf_range(.5, 1.5))
	if randi_range(0, 1):
		balaTransparente = false
		modulate.a8 = 255
	else:
		balaTransparente = true
		modulate.a8 = 100

func _process(delta):
	if !furaDePantalla:
		if global_rotation != 0:
			position += Vector2.from_angle(global_rotation) * delta * velocidad
		else:
			position.x -= delta * velocidad
	else:
		queue_free()
	if is_in_group("Contra tornado"):
		look_at(GLOBAL.siguenteTornadoPocicion)

func _on_en_pantalla_screen_exited():
	furaDePantalla = true

func _on_camvio_timeout():
	if balaTransparente:
		balaTransparente = false
		modulate.a8 = 255
	else:
		balaTransparente = true
		modulate.a8 = 100
	$Camvio.start(randf_range(.5, 1.5))

func _on_area_entered(area):
	if area.name == "Ataque jugador" && !balaTransparente:
		var indise: int = randi_range(0, 2)
		velocidad *= -1
		remove_from_group("Bala")
		add_to_group("Ataque jugador")
		if indise == 0:
			global_rotation = 0
		elif  indise == 1:
			global_rotation = randf_range(-3, -3.5)
