extends CharacterBody2D

@onready var forma: AnimatedSprite2D = $Forma
@onready var cañon: AnimatedSprite2D = $"Cañon/Cañon"
@export var balaEsena: PackedScene
@export var piedraEsena: PackedScene
var vida: int = 3
var xV: float
var a: float
var b: float
var c: float
var p: float

func _ready():
	GLOBAL.jefe = $"."
	cañon.global_position = $"Cañon".global_position
	forma.play("Normal")
	$"Mira 1/Ataque 1".start(randf_range(1, 3))
	$"Mira 2/Ataque 2".start(randf_range(1, 3))
	$"Mira 3/Ataque 3".start(randf_range(1, 3))
	$"Mira 4/Ataque 4".start(randf_range(1, 3))
	$"Cañon/Ataque cañon".start(randf_range(5, 10))
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if GLOBAL.vidaJugador > 0:
		moviminetoCañon()
	if vida == 0:
		GLOBAL.cantidadDeEnemigos -= 1
		queue_free()

func moviminetoCañon():
	xV = ((($"Cañon".global_position.x - GLOBAL.jugador.global_position.x) / 2) + GLOBAL.jugador.global_position.x) + ((GLOBAL.jugador.global_position.y - $"Cañon".global_position.y) / 2)
	p = ((GLOBAL.jugador.global_position.x - xV)**2) / (GLOBAL.jugador.global_position.y - 40)
	a = 1 / p
	b = (2 * (-xV)) / p
	c = ((xV**2) / p) + 40
	cañon.global_position.y = a * ($"Cañon".global_position.x**2) + b * $"Cañon".global_position.x + c

func crearBalas(mira: Marker2D):
	if vida > 0:
		var bala = balaEsena.instantiate()
		bala.position = mira.global_position
		if randi_range(0, 1):
			mira.look_at(GLOBAL.jugador.global_position)
			if randi_range(0, 1):
				bala.rotation = randf_range(3, 3.5)
			else:
				bala.rotation = mira.global_rotation
		get_parent().add_child(bala)

func _on_ataque_1_timeout():
	crearBalas($"Mira 1")
	$"Mira 1/Ataque 1".start(randf_range(1, 3))

func _on_ataque_2_timeout():
	crearBalas($"Mira 2")
	$"Mira 2/Ataque 2".start(randf_range(1, 3))

func _on_ataque_3_timeout():
	crearBalas($"Mira 3")
	$"Mira 3/Ataque 3".start(randf_range(1, 3))

func _on_ataque_4_timeout():
	crearBalas($"Mira 4")
	$"Mira 4/Ataque 4".start(randf_range(1, 3))

func _on_ataque_cañon_timeout():
	if vida > 0:
		cañon.play("Ataque")
		await (cañon.animation_finished)
		var piedra = piedraEsena.instantiate()
		piedra.global_position = $"Cañon".global_position
		get_parent().add_child(piedra)
		$"Cañon/Enfriamiento".start(1)

func _on_enfriamiento_timeout():
	if vida > 0:
		cañon.play_backwards("Ataque")
		await (cañon.animation_finished)
		$"Cañon/Ataque cañon".start(randf_range(5, 10))

func _on_inpacto_area_entered(area):
	if area.is_in_group("Bala pilar"):
		vida -= 1
		area.queue_free()
	elif area.is_in_group("Ataque jugador"):
		area.queue_free()
