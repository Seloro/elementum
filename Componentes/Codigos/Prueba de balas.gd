extends Marker2D

@export var balasEscenas: Array
@onready var animacion: AnimatedSprite2D = $Forma
var atacar: bool = true
var x: float
var y: float
var xV: float
var p: float
var a: float
var b: float
var c: float

func _ready():
	animacion.global_position.x = global_position.x

func  _process(delta):
	x = GLOBAL.jugador.global_position.x
	y = GLOBAL.jugador.global_position.y
	xV = (((global_position.x - x) / 2) + x) + ((y - global_position.y) / 2)
	p = ((x - xV)**2) / (y - 40)
	a = 1 / p
	b = (2 * (-xV)) / p
	c = ((xV**2) / p) + 40
	$Forma.global_position.y = a * (position.x**2) + b * position.x + c

func _input(event):
	if Input.is_key_pressed(KEY_KP_0):
		visible = !visible
	if Input.is_key_pressed(KEY_P) && atacar:
		atacar = false
		animacion.play("Ataque")
		await (animacion.animation_finished)
		var bala = balasEscenas[0].instantiate()
		bala.position = global_position
		get_parent().add_child(bala)
		$Timer.start(1)
	if Input.is_key_pressed(KEY_O):
		var bala = balasEscenas[1].instantiate()
		bala.position = global_position
		bala.rotation = global_rotation
		get_parent().add_child(bala)
	if Input.is_key_pressed(KEY_I):
		var bala = balasEscenas[2].instantiate()
		bala.position = global_position
		get_parent().add_child(bala)

func _on_timer_timeout():
	animacion.play_backwards("Ataque")
	await (animacion.animation_finished)
	atacar = true
