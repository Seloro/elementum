extends CharacterBody2D

@onready var animacion: AnimatedSprite2D =  $AnimatedSprite2D
@export var balaEsena: PackedScene
var vida: int = 3

func _ready():
	$"Mira 1/Ataque 1".start(randf_range(1, 3))
	$"Mira 2/Ataque 2".start(randf_range(1, 3))
	$"Mira 3/Ataque 3".start(randf_range(1, 3))
	$"Mira 4/Ataque 4".start(randf_range(1, 3))
	$"Mira 5/Ataque 5".start(randf_range(1, 3))
	$"Mira 6/Ataque 6".start(randf_range(1, 3))
	GLOBAL.jefe = $Nucleo/CollisionShape2D
	animacion.play("Normal")
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if vida <= 0:
		GLOBAL.cantidadDeEnemigos -= 1
		queue_free()

func creaBalas(mira: Marker2D):
	if vida > 0 && GLOBAL.vidaJugador > 0:
		var bala = balaEsena.instantiate()
		bala.position = mira.global_position
		if randi_range(0, 1):
			mira.look_at(GLOBAL.jugador.global_position)
			if randi_range(0, 1):
				bala.rotation = randf_range(3, 3.5)
			else:
				bala.rotation = mira.global_rotation
		get_parent().add_child(bala)

func _on_ataque_1_timeout():
	creaBalas($"Mira 1")
	$"Mira 1/Ataque 1".start(randf_range(1, 3))

func _on_ataque_2_timeout():
	creaBalas($"Mira 2")
	$"Mira 2/Ataque 2".start(randf_range(1, 3))

func _on_ataque_3_timeout():
	creaBalas($"Mira 3")
	$"Mira 3/Ataque 3".start(randf_range(1, 3))

func _on_ataque_4_timeout():
	creaBalas($"Mira 4")
	$"Mira 4/Ataque 4".start(randf_range(1, 3))

func _on_ataque_5_timeout():
	creaBalas($"Mira 5")
	$"Mira 5/Ataque 5".start(randf_range(1, 3))

func _on_ataque_6_timeout():
	creaBalas($"Mira 6")
	$"Mira 6/Ataque 6".start(randf_range(1, 3))

func _on_inpacto_area_entered(area):
	if area.is_in_group("Ataque jugador"):
		area.queue_free()

func _on_nucleo_area_entered(area):
	if area.is_in_group("Contra tornado"):
		area.queue_free()
		vida -= 1
		GLOBAL.indiceTornado = 0
