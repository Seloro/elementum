extends StaticBody2D

@onready var formas: Node2D = $Formas
var roto: bool
var quebrado: bool
@export var balaEsena: PackedScene
var areaInfo
var areaPiedra

func _process(delta):
	if areaInfo != null:
		if areaInfo.name == "Ataque jugador" && quebrado:
			var bala = balaEsena.instantiate()
			bala.position = global_position
			bala.look_at(GLOBAL.jefe.global_position)
			get_parent().add_child(bala)
			roto = true
			quebrado = false
			$Formas/PilarDePiedraQuebrado.visible = false
	if areaPiedra != null:
		if areaPiedra.is_in_group("Quieto") && !roto:
			$Formas/PilarDePiedra.visible = false
			quebrado = true
	if roto:
		top_level = false
		formas.modulate.a8 = 255

func _on_canvio_de_visivilidad_area_entered(area):
	if area.is_in_group("Jugador") && !roto:
		formas.modulate.a8 = 50
		top_level = true

func _on_canvio_de_visivilidad_area_exited(area):
	if area.is_in_group("Jugador") && !roto:
		formas.modulate.a8 = 255
		top_level = false

func _on_inpacto_area_entered(area):
	if area.is_in_group("Piedra grande") && !roto:
		areaPiedra = area
	if area.name == "Ataque jugador" && quebrado:
		var bala = balaEsena.instantiate()
		bala.position = global_position
		get_parent().add_child(bala)
		roto = true
		quebrado = false
		$Formas/PilarDePiedraQuebrado.visible = false
	if area.name == "Ataque":
		areaInfo = area

func _on_inpacto_area_exited(area):
	if area.name == "Ataque" || area.name == "Ataque jugador":
		areaInfo = null
	if area.is_in_group("Piedra grande"):
		areaPiedra = null
