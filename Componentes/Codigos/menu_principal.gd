extends Control

@onready var panelInicial: Panel = $Inicio
@onready var panelElecionNiveles: Panel = $"Eleccion de niveles"

func _ready():
	GLOBAL.rutasRandom = false
	panelElecionNiveles.visible = false
	GLOBAL.indiceNivel = 0

func _on_jugar_button_down():
	panelElecionNiveles.visible = true
	panelInicial.visible = false

func _on_volver_button_down():
	panelElecionNiveles.visible = false
	panelInicial.visible = true

func _on_tierra_button_down():
	GLOBAL.indiseRuta = 0
	get_tree().change_scene_to_file("res://Componentes/Escenas/Mapa_pruebas.tscn")

func _on_agua_button_down() -> void:
	GLOBAL.indiseRuta = 1
	get_tree().change_scene_to_file("res://Componentes/Escenas/Mapa_pruebas.tscn")

func _on_aire_button_down() -> void:
	GLOBAL.indiseRuta = 2
	get_tree().change_scene_to_file("res://Componentes/Escenas/Mapa_pruebas.tscn")

func _on_fuego_button_down() -> void:
	GLOBAL.indiseRuta = 3
	get_tree().change_scene_to_file("res://Componentes/Escenas/Mapa_pruebas.tscn")

func _on_boton_random_button_down() -> void:
	GLOBAL.rutasRandom = true
	get_tree().change_scene_to_file("res://Componentes/Escenas/Mapa_pruebas.tscn")

func _on_salir_button_down():
	get_tree().quit()
