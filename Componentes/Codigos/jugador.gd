extends CharacterBody2D

const VELOCIDAD = 150
var animacion: AnimatedSprite2D
var indiceDireccion: int
var accion: bool
var ataqueArea: Area2D
var animacionNombre: String
var animacionEnMovimiento: bool
var congelado: bool
@onready var dañoSonaArdiente: Timer = $"Daño de sona ardiente"
var enAreaArdiente: bool
@onready var salto: Timer = $"Tiempo de salto"
var areaAusiliar: Area2D
var inmortalidad: bool

func _ready():
	animacion = $Forma
	ataqueArea = $Ataque
	GLOBAL.jugador = $"."
	GLOBAL.vidaJugador = 100
	dañoSonaArdiente.stop()
	actualizacionDeUI()
	inmortalidad = false

func _process(delta):
	movimiento(delta)
	ataque()
	ataquesElevados()
	actualizacionDeUI()
	cambiarInmortalidad()
	muerte()
	if z_index !=0:
		dañoSonaArdiente.stop()
	elif enAreaArdiente && dañoSonaArdiente.is_stopped():
		dañoSonaArdiente.start()
	if areaAusiliar != null:
		if areaAusiliar.is_in_group("Quieto"):
			GLOBAL.vidaJugador -= 20
			areaAusiliar = null

func movimiento(delta):
	if !accion && !congelado && GLOBAL.vidaJugador > 0:
		if Input.is_action_pressed("Avajo"):
			position.y += VELOCIDAD * delta
			animacionNombre = "Correr avajo"
			indiceDireccion = 0
			animacionEnMovimiento = true
		elif Input.is_action_pressed("Arriba"):
			position.y -= VELOCIDAD * delta
			animacionNombre = "Correr arriba"
			indiceDireccion = 1
			animacionEnMovimiento = true
		if Input.is_action_pressed("Izquieda"):
			animacion.flip_h = true
			position.x -= VELOCIDAD * delta
			animacionNombre = "Correr lateral"
			indiceDireccion = 2
			animacionEnMovimiento = true
		elif Input.is_action_pressed("Derecha"):
			animacion.flip_h = false
			position.x += VELOCIDAD * delta
			animacionNombre = "Correr lateral"
			indiceDireccion = 2
			animacionEnMovimiento = true
		if animacionEnMovimiento:
			animacion.play(animacionNombre)
	if !Input.is_anything_pressed() && !accion && !congelado && GLOBAL.vidaJugador > 0:
		velocity = Vector2.ZERO
		animacionEnMovimiento = false
		if indiceDireccion == 0:
			animacion.play("Espera avajo")
		if indiceDireccion == 1:
			animacion.play("Espera arriba")
		if indiceDireccion == 2:
			animacion.play("Espera lateral")
	if Input.is_action_pressed("Salto") && salto.is_stopped() && !accion && !congelado && GLOBAL.vidaJugador > 0:
		z_index = 1
		salto.start()
	move_and_slide()

func ataque():
	if Input.is_action_pressed("Ataque") && !congelado && GLOBAL.vidaJugador > 0:
		accion = true
		animacion.flip_h = false
		indiceDireccion = 2
		ataqueArea.name = "Ataque jugador"
		animacion.play("Ataque")
		await (animacion.animation_finished)
		ataqueArea.name = "Ataque"
		accion = false

func _on_impacto_area_entered(area):
	if !inmortalidad:
		if area.is_in_group("Bala"):
			area.queue_free()
			GLOBAL.vidaJugador -= 10
		if area.is_in_group("Bala jefe viento"):
			area.queue_free()
			GLOBAL.vidaJugador -= 4
		if area.is_in_group("Caranbano"):
			animacion.modulate = Color8(0, 170, 255, 255)
			animacion.pause()
			area.queue_free()
			GLOBAL.vidaJugador -= 10 - 2 * GLOBAL.indiceNivel
			if ataqueArea.name == "Ataque jugador":
				ataqueArea.name = "Congelado"
			if !congelado:
				congelado = true
				$Congelado.start(randf_range(1, 1.5))
		if area.is_in_group("Sona ardiente") && z_index == 0:
			GLOBAL.vidaJugador -= 2
			dañoSonaArdiente.start()
			enAreaArdiente = true
		if area.is_in_group("Piedra grande"):
			areaAusiliar = area
	if area.is_in_group("TornadoEntorno"):
		top_level = false

func _on_congelado_timeout():
	animacion.modulate = Color8(255, 255, 255, 255)
	if ataqueArea.name == "Congelado":
		ataqueArea.name = "Ataque jugador"
	animacion.play()
	congelado = false

func _on_daño_de_sona_ardiente_timeout():
	GLOBAL.vidaJugador -= 2

func _on_impacto_area_exited(area):
	if area.is_in_group("Sona ardiente"):
		dañoSonaArdiente.stop()
		enAreaArdiente = false
	if area.is_in_group("Piedra grande"):
		areaAusiliar = null
	if area.is_in_group("TornadoEntorno"):
		top_level = true

func _on_tiempo_de_salto_timeout():
	z_index = 0

func ataquesElevados():
	if GLOBAL.arriba:
		ataqueArea.add_to_group("Arriba")
	else:
		ataqueArea.remove_from_group("Arriba")

func actualizacionDeUI():
	$"CanvasLayer/UI Jugador/ProgressBar".value = GLOBAL.vidaJugador

func cambiarInmortalidad():
	if Input.is_key_label_pressed(KEY_I):
		inmortalidad = !inmortalidad

func muerte():
	if GLOBAL.vidaJugador <= 0:
		animacion.play("Muerte")
		await (animacion.animation_finished)
		queue_free()
