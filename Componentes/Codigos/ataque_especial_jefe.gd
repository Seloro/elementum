extends Marker2D

@onready var forma: AnimatedSprite2D = $Forma
var dañoPermitido: bool
var ataqueJugador: Area2D

func _ready():
	forma.play("Ataque")
	await (forma.animation_finished)
	$Volver.start(randf_range(2, 4))
	dañoPermitido = true

func _process(delta):
	deteccionDeDaño()

func _on_volver_timeout():
	dañoPermitido = false
	forma.play_backwards("Ataque")
	await (forma.animation_finished)
	GLOBAL.ataqueEspecialFuego = true
	queue_free()

func _on_area_de_atras_area_entered(area):
	if area.is_in_group("Jugador"):
		forma.modulate.a8 = 50
		top_level = true

func _on_area_de_atras_area_exited(area):
	if area.is_in_group("Jugador"):
		forma.modulate.a8 = 255
		top_level = false

func _on_inpacto_area_entered(area):
	if area.name == "Ataque jugador" || area.name == "Ataque":
		ataqueJugador = area

func _on_inpacto_area_exited(area):
	if area.name == "Ataque jugador" || area.name == "Ataque":
		ataqueJugador = null

func deteccionDeDaño():
	if dañoPermitido && ataqueJugador != null:
		if ataqueJugador.name == "Ataque jugador" && ataqueJugador.is_in_group("Arriba"):
			GLOBAL.vidaJefeFuego -= 1
			if GLOBAL.vidaJefeFuego == 0:
				queue_free()
			$Volver.stop()
			dañoPermitido = false
			forma.play_backwards("Ataque")
			await (forma.animation_finished)
			GLOBAL.ataqueEspecialFuego = true
			queue_free()
