extends Area2D

@onready var animacion: AnimatedSprite2D = $Forma
@export var velocidad: float = 1
@onready var transportador: PathFollow2D = $Camino/Transporte
@onready var temp: Timer = $Temp
var puedoResibir: bool = true
var bala: Area2D
@export var indisePropio: int

func _ready():
	animacion.speed_scale = velocidad
	animacion.play("Normal")

func _process(delta):
	if indisePropio == GLOBAL.indiceTornado:
		GLOBAL.siguenteTornadoPocicion = position
		puedoResibir = true
	if !puedoResibir:
		transportador.progress += delta * 500 * (indisePropio + 1)
		bala.global_position = transportador.global_position
	else:
		transportador.progress = 0
	if GLOBAL.indiceTornado >= 4:
		GLOBAL.siguenteTornadoPocicion = GLOBAL.jefe.global_position

func _on_area_entered(area):
	if puedoResibir && indisePropio == GLOBAL.indiceTornado:
		if area.is_in_group("Bala jefe viento") && indisePropio == 0:
			#area.remove_from_group("Bala")
			area.remove_from_group("Bala jefe viento")
			area.add_to_group("Contra tornado")
			bala = area
			puedoResibir = false
			GLOBAL.indiceTornado += 1
			temp.start(10 - indisePropio * 2)
		if area.is_in_group("Contra tornado") && indisePropio == GLOBAL.indiceTornado:
			bala = area
			puedoResibir = false
			GLOBAL.indiceTornado += 1
			temp.start(10 - indisePropio * 2)
	if area.is_in_group("Contra tornado"):
		if area.global_position.y < global_position.y:
			area.top_level = false
			top_level = true
		else:
			area.top_level = true
			top_level = false

func _on_area_exited(area):
	if area.is_in_group("Contra tornado"):
		top_level = false

func _on_temp_timeout():
	puedoResibir = true

func _on_vista_area_entered(area):
	if area.is_in_group("Jugador"):
		$Forma.modulate.a8 = 50

func _on_vista_area_exited(area):
	if area.is_in_group("Jugador"):
		$Forma.modulate.a8 = 255
