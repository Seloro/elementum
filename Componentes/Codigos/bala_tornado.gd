extends Area2D

var velocidad: int
var balaTransparente: bool
var forma: Sprite2D

func _ready():
	velocidad = randi_range(250, 500)
	forma = $BalaViento
	$Timer.start(randf_range(.5, 1.5))
	if randi_range(0, 1):
		balaTransparente = false
		forma.modulate.a8 = 255
	else:
		balaTransparente = true
		forma.modulate.a8 = 100

func _process(delta):
	if position.x <= 1000 && position.x >= -1000:
		if global_rotation != 0:
			position += Vector2.from_angle(global_rotation) * delta * velocidad
		else:
			position.x -= delta * velocidad
	else: 
		queue_free()

func _on_area_entered(area):
	if area.name == "Ataque jugador" && !balaTransparente:
		var indise: int = randi_range(0, 2)
		velocidad *= -1
		remove_from_group("Bala")
		add_to_group("Ataque jugador")
		if indise == 0:
			global_rotation = 0
		elif  indise == 1:
			global_rotation = randf_range(-3, -3.5)
	if area.is_in_group("Ataque jugador") && is_in_group("Bala"):
		queue_free()
	if area.is_in_group("Bala") && is_in_group("Ataque jugador"):
		queue_free()

func _on_timer_timeout():
	if balaTransparente:
		balaTransparente = false
		forma.modulate.a8 = 255
	else:
		balaTransparente = true
		forma.modulate.a8 = 100
	$Timer.start(randf_range(.5, 1.5))
