extends Area2D

var velocidad: int
var x: float
var y: float
var temp: float
var a: float
var b: float
var c: float
var p: float
var xV: float

func _ready():
	modulate.a8 = 0
	velocidad = randi_range(600, 800)
	x = GLOBAL.jugador.global_position.x
	y = GLOBAL.jugador.global_position.y
	xV = (((global_position.x - x) / 2) + x) + ((y - global_position.y) / 2)
	p = ((x - xV)**2) / (y - 40)
	a = 1 / p
	b = (2 * (-xV)) / p
	c = ((xV**2) / p) + 40

func _physics_process(delta):
	if global_position.x > x:
		modulate.a8 = 255
		global_position.x -= velocidad * delta
		global_position.y = a * (global_position.x**2) + b * global_position.x + c
	else:
		remove_from_group("Piedra grande")
		add_to_group("Quieto")
		temp += delta
		if temp >= 1:
			queue_free()
