extends Area2D

var velocidad: int
var signo: bool

func _ready():
	velocidad = randi_range(250, 500)
	signo = randi_range(0, 1)

func _process(delta):
	position += Vector2.from_angle(global_rotation) * delta * velocidad
	if signo:
		$BloqueDeHielo.rotation += delta
	else:
		$BloqueDeHielo.rotation -= delta
