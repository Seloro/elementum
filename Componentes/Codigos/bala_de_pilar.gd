extends Area2D

var velocidad: int = 400

func _process(delta):
	position += Vector2.from_angle(global_rotation) * delta * velocidad
