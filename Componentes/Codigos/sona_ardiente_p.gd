extends Area2D

@export var sonasHijas: PackedScene
@onready var velocidad: int = randi_range(150, 200)
var movimiento: bool = true

func _process(delta):
	if position.x <= 1000 && position.x >= -1000:
		if movimiento:
			position += Vector2.from_angle(global_rotation) * delta * velocidad
	else:
		queue_free()

func _on_timer_timeout():
	movimiento = false
	for i in 3:
		var hijo = sonasHijas.instantiate()
		hijo.position = global_position
		hijo.scale = scale / 2
		if i == 0:
			hijo.rotation = $"Hijo 1".global_rotation
		if i == 1:
			hijo.rotation = $"Hijo 2".global_rotation
		if i == 2:
			hijo.rotation = $"Hijo 3".global_rotation
		get_parent().add_child(hijo)
	$Muerte.start(20)

func _on_body_entered(body):
	$Timer.start(randf_range(.5, 1))

func _on_muerte_timeout():
	queue_free()
