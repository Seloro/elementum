extends Node2D

@export var enemigos: Array[Array]

func _ready():
	$Camera2D/CanvasLayer.visible = false
	GLOBAL.cantidadDeEnemigos = 0
	instanciarEnemigos()
	$Camera2D/CanvasLayer/Menus/Indicacion.text = "Ronda " + str(GLOBAL.indiceNivel + 1)

func _process(delta):
	if (GLOBAL.cantidadDeEnemigos <= 0) || GLOBAL.vidaJugador <= 0:
		if GLOBAL.indiceNivel >= 3:
			$Camera2D/CanvasLayer/Menus/Seguir.text = "Seguir"
		elif GLOBAL.vidaJugador <= 0:
			$Camera2D/CanvasLayer/Menus/Seguir.text = "Salir"
			$Camera2D/CanvasLayer/Menus/Resultado.text = "Perdiste"
		$Camera2D/CanvasLayer.visible = true
	saltarNivel()

func instanciarEnemigos():
	rutaRandom()
	if GLOBAL.indiceNivel < 2:
		cargarEnemigo(GLOBAL.indiceNivel)
	elif GLOBAL.indiceNivel == 2:
		cargarEnemigo(1)
		rutaRandom()
		cargarEnemigo(1, -1)
	else:
		prepararEsenario()
		cargarEnemigo(2)

func rutaRandom():
	if GLOBAL.rutasRandom:
		GLOBAL.indiseRuta = randi_range(0, enemigos.size() - 1)

func cargarEnemigo(indice, direccion = 1):
	var enemigo = enemigos[GLOBAL.indiseRuta][indice].instantiate()
	if GLOBAL.indiceNivel < 2:
		enemigo.position = $"Inicio enemigo".global_position + Vector2.UP * randi_range(-100, 101)
	if GLOBAL.indiceNivel == 2:
		enemigo.position = $"Inicio enemigo".global_position + Vector2.UP * 100 * direccion
	elif GLOBAL.indiceNivel == 3 && GLOBAL.indiseRuta == 0:
		enemigo.position = $"Inicio enemigo".global_position + Vector2.UP * 50
	elif GLOBAL.indiceNivel == 3 && GLOBAL.indiseRuta == 1:
		enemigo.position = $"Inicio enemigo".global_position + Vector2.LEFT * 100
	elif GLOBAL.indiceNivel == 3 && GLOBAL.indiseRuta == 2:
		enemigo.position = $"Inicio enemigo".global_position+ Vector2.UP * 50
	else:
		enemigo.position = $"Inicio enemigo".global_position+ Vector2.UP * 50
	add_child(enemigo)

func prepararEsenario():
	if GLOBAL.indiseRuta == 0:
		$Pilares.position = Vector2.ZERO
		$Pilares.visible = true
	elif GLOBAL.indiseRuta == 2:
		$Tornados.position = Vector2.ZERO
		$Tornados.visible = true
	elif  GLOBAL.indiseRuta == 3:
		$Plataformas.position = Vector2.ZERO
		$Plataformas.visible = true

func _on_seguir_button_down():
	GLOBAL.indiceNivel += 1
	if GLOBAL.indiceNivel >= 4 || GLOBAL.vidaJugador <= 0:
		get_tree().change_scene_to_file("res://Componentes/Escenas/menu_principal.tscn")
	else:
		get_tree().change_scene_to_file("res://Componentes/Escenas/Mapa_pruebas.tscn")

func _on_salir_button_down():
	get_tree().change_scene_to_file("res://Componentes/Escenas/menu_principal.tscn")

func saltarNivel():
	if Input.is_key_pressed(KEY_P):
		$Camera2D/CanvasLayer.visible = true
