extends CharacterBody2D

const VELOCIDAD = 50
var animacion: AnimatedSprite2D
var movimiento: bool
var x: int
var y: int
@export var balaEsena: PackedScene
var vida: float = 1

func _ready():
	animacion = $Forma
	$Ataque.start(randf_range(1, 3))
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if movimiento && vida > 0:
		animacion.play("Movimiento")
		position += Vector2(x, y) * delta * VELOCIDAD
	elif vida > 0:
		animacion.play("Espera")
	move_and_slide()
	if vida <= 0:
		animacion.play("Muerte")
		await (animacion.animation_finished)
		GLOBAL.cantidadDeEnemigos -= 1
		queue_free()

func _on_movimineto_timeout():
	movimiento = !movimiento

func _on_cambio_de_direccion_timeout():
	x = randi_range(-1, 1)
	y = randi_range(-1, 1)

func _on_ataque_timeout():
	if vida > 0 && GLOBAL.jugador != null:
		var bala = balaEsena.instantiate()
		bala.position = $Mira.global_position
		if randi_range(0, 1):
			$Mira.look_at(GLOBAL.jugador.global_position)
			bala.rotation = $Mira.global_rotation
		get_parent().add_child(bala)
		$Ataque.start(randf_range(1, 3))

func _on_impacto_area_entered(area):
	if area.is_in_group("Ataque jugador"):
		area.queue_free()
		vida -= 1
