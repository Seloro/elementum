extends CharacterBody2D

@onready var forma: AnimatedSprite2D = $Forma
@export var balaNormal: PackedScene
@export var balaDos: PackedScene
@export var balaAtaqueEspecial: PackedScene
var vida: int = 3
var atacar: bool
var espera: bool
var sumergir: bool
var sumergido: bool
var reiniciar: bool
var esperaDeBalaDos: bool

func _ready():
	forma.play("Espera")
	espera = true
	$Ataque.start(randf_range(1, 3))
	$Sumergirse.start(randf_range(8, 12))
	GLOBAL.ataqueEspecialFuego = false
	GLOBAL.vidaJefeFuego = 3
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if GLOBAL.vidaJefeFuego > 0:
		esperando()
		sumergirse()
		reinicio()
	else:
		GLOBAL.cantidadDeEnemigos -= 1

func _on_ataque_timeout():
	if !sumergir:
		if atacar:
			forma.play_backwards("Atacar")
			await (forma.animation_finished)
			espera = true
			atacar = false
			$Ataque.start(randf_range(1, 3))
			$Sumergirse.paused = false
		else:
			$Sumergirse.paused = true
			atacar = true
			espera = false
			forma.play("Atacar")
			await (forma.animation_finished)
			var num: int
			num = randi_range(2, 5)
			for i in range(num):
				var bala = balaNormal.instantiate()
				bala.global_position = $Mira.global_position
				bala.scale *= 2
				if randi_range(0, 1):
					bala.rotation = randf_range(2.5, 3)
				else:
					$Mira.look_at(GLOBAL.jugador.global_position)
					bala.rotation = $Mira.global_rotation
				get_parent().add_child(bala)
			if randi_range(0,1) && !esperaDeBalaDos:
				var bala = balaDos.instantiate()
				bala.global_position = $Mira.global_position
				$Mira.look_at(GLOBAL.jugador.global_position)
				bala.rotation = $Mira.global_rotation
				get_parent().add_child(bala)
				esperaDeBalaDos = true
				$"Temporizador bala dos".start(20)
			$Ataque.start(.25)

func esperando():
	if espera && !sumergir:
		forma.play("Espera")

func sumergirse():
	if sumergir && espera && !atacar && !sumergido:
		forma.play("Sumergirse")
		await (forma.animation_finished)
		sumergido = true
		espera = false
		$"Ataque especial".start(randf_range(1.5, 3))
		reiniciar = true

func _on_sumergirse_timeout():
	sumergir = true

func _on_ataque_especial_timeout():
	var bala = balaAtaqueEspecial.instantiate()
	bala.global_position = eleccionDePosicion()
	get_parent().add_child(bala)
	GLOBAL.ataqueEspecialFuego = false

func reinicio():
	if GLOBAL.ataqueEspecialFuego:
		reiniciar = false
		GLOBAL.ataqueEspecialFuego = false
		forma.play_backwards("Sumergirse")
		await (forma.animation_finished)
		sumergido = false
		espera = true
		sumergir = false
		$Ataque.start(randf_range(1, 3))
		$Sumergirse.start(randf_range(8, 12))

func eleccionDePosicion():
	var indice: int = randi_range(0, 2)
	while  GLOBAL.posicionesJefeFuegoAtaqueEspecial[indice] == Vector2.ZERO:
		indice  = randi_range(0, 2)
	return GLOBAL.posicionesJefeFuegoAtaqueEspecial[indice]

func _on_destrulle_balas_area_entered(area):
	if area.is_in_group("Ataque jugador"):
		area.queue_free()

func _on_temporizador_bala_dos_timeout():
	esperaDeBalaDos = false
