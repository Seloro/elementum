extends RigidBody2D

var velocidad: int
var x: float
var y: float
var cero: bool
var xInicial: float
var yInicial: float

func _ready():
	velocidad = randi_range(250, 500)
	x = GLOBAL.jugador.global_position.x
	y = GLOBAL.jugador.global_position.y
	xInicial = position.x
	yInicial = position.y

func _physics_process(delta):
	if global_position.x > x:
		global_position.x -= velocidad * delta
		if global_position.x > (((xInicial - x) / 2) + x) + (y - yInicial) / 2: 
			#gravity_scale = 0
			global_position.y -= velocidad / 40
		else:
			#gravity_scale = 1
			if !cero:
				linear_velocity.y = 0
				cero = true
	else:
		linear_velocity = Vector2.ZERO
		gravity_scale = 0
