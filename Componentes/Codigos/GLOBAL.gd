class_name GLOBAL
extends Node

static var jugador
static var vidaJugador: float
static var jefe
static var contadorOla: int
static var contadorContrataque: int = 1
static var siguenteTornadoPocicion: Vector2
static var indiceTornado: int
static var arriba: bool
static var ataqueEspecialFuego: bool
static var vidaJefeFuego: int
static var posicionesJefeFuegoAtaqueEspecial = [0,1,2]
static var rutasRandom: bool
static var indiseRuta: int
static var indiceNivel: int
static var cantidadDeEnemigos: int
