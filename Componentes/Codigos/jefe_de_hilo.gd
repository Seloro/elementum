extends CharacterBody2D

@export var olaEsena: PackedScene
var vida: int = 12
@onready var hojoD: AnimatedSprite2D = $"Hojo derecho"
@onready var hojoI: AnimatedSprite2D = $"Hojo izquiedo"
@onready var parpadeo: Timer = $Parpadeo
@onready var cascadaAnimacion: AnimatedSprite2D = $"Cascadas/Cascada animacion"
var limite: int
var indiceCascada: int
@export var bloqueHielo: PackedScene
@export var cascadaEsena: PackedScene

func _ready():
	parpadeo.start(randf_range(1, 2))
	cascadaAnimacion.play("0")
	$Cascadas/CascadaConjelada.visible = false
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if vida == 0:
		var cascada = cascadaEsena.instantiate()
		cascada.position = $Cascadas.global_position
		get_parent().add_child(cascada)
		GLOBAL.cantidadDeEnemigos -= 1
		queue_free()
	if GLOBAL.contadorOla == 40:
		if GLOBAL.contadorContrataque != 10:
			var ola = olaEsena.instantiate()
			ola.position = global_position
			ola.position.x += 200
			get_parent().add_child(ola)
			GLOBAL.contadorOla = 0
			$Enfriamiento.start(4)
		else:
			GLOBAL.contadorContrataque = 0
			limite = 0
			indiceCascada = 0
			GLOBAL.contadorOla = 0
			crear_Bloque($"Miras/Mira 1")
			crear_Bloque($"Miras/Mira 2")
			crear_Bloque($"Miras/Mira 3")
			crear_Bloque($"Miras/Mira 4")
			$Cascadas/CascadaConjelada.visible = false
			$"Cascadas/Cascada animacion".visible = true
	if GLOBAL.contadorContrataque == 10:
		cascadaAnimacion.visible = false
		$Cascadas/CascadaConjelada.visible = true
	else:
		if GLOBAL.contadorContrataque == limite:
			limite += 10
			cascadaAnimacion.play(str(indiceCascada))
			indiceCascada += 1

func _on_parpadeo_timeout():
	hojoD.play("Normal")
	hojoI.play("Normal")
	await (hojoI.animation_finished)
	parpadeo.start(randf_range(1, 2))

func _on_enfriamiento_timeout():
	GLOBAL.contadorOla = 0

func _on_inpacto_area_entered(area):
	if area.is_in_group("Ataque jugador"):
		if GLOBAL.contadorContrataque < 10:
			GLOBAL.contadorContrataque += 1
		elif GLOBAL.contadorContrataque > 10:
			GLOBAL.contadorContrataque = 10
		area.queue_free()

func crear_Bloque(mira: Marker2D):
	var bloque = bloqueHielo.instantiate()
	bloque.position = mira.global_position
	mira.look_at($".".position)
	bloque.rotation = mira.rotation
	get_parent().add_child(bloque)

func _on_impacto_2_area_entered(area):
	if area.is_in_group("Bloque de hielo"):
		area.queue_free()
		vida -= 1
