extends CharacterBody2D

var velocidad: int = 50
var animacion: AnimatedSprite2D
var movimiento: bool
var x: int
var y: int
@export var bolaDeFuego: PackedScene
@export var sonaArdiente: PackedScene
var vida: float = 1
var inpacto: Area2D
var ataqueDos: bool

func _ready():
	animacion = $Forma
	$Ataque.start(randf_range(1, 3))
	$"Enfriamiento de ataque 2".start(10)
	inpacto = $Inpacto
	animacion.play("Normal")
	GLOBAL.cantidadDeEnemigos += 1

func _process(delta):
	if movimiento && vida > 0:
		position += Vector2(x, y) * delta * velocidad
	move_and_slide()
	if vida <= 0:
		GLOBAL.cantidadDeEnemigos -= 1
		queue_free()

func _on_movimineto_timeout():
	movimiento = !movimiento

func _on_cambio_de_direccion_timeout():
	x = randi_range(-1, 1)
	y = randi_range(-1, 1)

func _on_ataque_timeout():
	if vida > 0 && GLOBAL.vidaJugador > 0:
		var bala
		$Mira.look_at(GLOBAL.jugador.global_position)
		if ataqueDos:
			bala = sonaArdiente.instantiate()
			bala.rotation = $Mira.global_rotation
			ataqueDos = false
			$"Enfriamiento de ataque 2".start(10)
		else:
			bala = bolaDeFuego.instantiate()
			if randi_range(0, 1):
				if randi_range(0, 1):
					bala.rotation = randf_range(3, 3.5)
				else:
					bala.rotation = $Mira.global_rotation
		bala.position = $Mira.global_position
		get_parent().add_child(bala)
		$Ataque.start(randf_range(1, 3))

func _on_enfriamiento_de_ataque_2_timeout():
	ataqueDos = true

func _on_inpacto_area_entered(area):
	if area.is_in_group("Ataque jugador"):
		area.queue_free()
		vida -= 1
